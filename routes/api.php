<?php

use Illuminate\Http\Request;

Route::get('/', function() {
    return 'OK';
});

Route::post('/', 'FileUploadController@postFiles');