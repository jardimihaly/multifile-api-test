<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class FileUploadController extends Controller
{
    public function postFiles(Request $r)
    {
        $r->validate([
            'file' => 'required|file|mimes:zip,gzip'
        ]);

        $extractPath = '../storage/app/extracted';

        $file = $r->file('file');
        $file->storeAs('compressed', 'test.zip');

        $file = Storage::get('/compressed/test.zip');

        $zip = new ZipArchive();

        $res = $zip->open('../storage/app/compressed/test.zip');

        if($res === true)
        {
            $zip->extractTo($extractPath);
            $zip->close();
        }
        else
        {
            return response('error: ' . $res, 500);    
        }

        $files = array_filter(scandir($extractPath), function($e) { return $e !== '.' && $e !== '..'; });

        foreach ($files as $file) {
            unlink($extractPath . '/' . $file);
        }

        unlink('../storage/app/compressed/test.zip');

        return response(array_values($files), 200);
    }
}
